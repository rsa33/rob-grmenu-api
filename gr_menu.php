<?
require_once(getenv("HOME") . "/lib/html_parse.php");
header("Content-type: application/json");
$dom = new simple_html_dom();
$base = "https://www.robinson.cam.ac.uk/college-life/garden-restaurant-menu";
$flags = array(
    "alcohol" => "<img src=\"/images/catering/icons/a.gif\" width=\"20\" height=\"20\" alt=\"(a)\" />",
    "healthy" => "<img src=\"/images/catering/icons/h.gif\" width=\"20\" height=\"20\" alt=\"(h)\" />",
    "fairtrade" => "<img src=\"/images/catering/icons/f.gif\" width=\"20\" height=\"20\" alt=\"(f)\" />",
    "nuts" => "<img src=\"/images/catering/icons/n.gif\" width=\"20\" height=\"20\" alt=\"(n)\" />",
    "vegan" => "<img src=\"/images/catering/icons/v.gif\" width=\"20\" height=\"20\" alt=\"(v)\" />"
);
$off = isset($_REQUEST["off"]) ? (int) $_REQUEST["off"] : 0;
$lim = isset($_REQUEST["lim"]) ? (int) $_REQUEST["lim"] : 1;
if ($lim < 1) {
    $lim = 1;
}
$root = array(
    "off" => $off,
    "lim" => $lim,
    "menus" => array()
);
for ($i = 0; $i < $lim; $i++) {
    $time = time() + (($off + $i) * 60 * 60 * 24);
    $dom->load_file("${base}?date=" . date("Y-m-d", $time));
    $day = array("date" => date("d/m/Y", $time));
    $tables = $dom->find("table");
    $blocks = $tables[2]->find("td");
    $sets = array("lunch", "dinner");
    $x = 0;
    foreach ($blocks as $block) {
        $rows = $block->find("table tr");
        if ($rows) {
            $list = array();
            foreach ($rows as $row) {
                $cells = $row->find("td");
                $text = str_replace("<i>", "", str_replace("</i>", "", $cells[0]->plaintext));
                $item = array("flags" => array());
                foreach ($flags as $flag => $repl) {
                    if (strpos($text, $repl) !== false) {
                        $text = str_replace($repl, "", $text);
                        array_push($item["flags"], $flag);
                    }
                }
                if (preg_match("/Vegan/", $text)) {
                    $text = preg_replace("/Vegan[:;]?\s*/", "", $text);
                    if (!in_array("vegan", $item["flags"])) {
                        array_push($item["flags"], "vegan");
                    }
                }
                $text = str_replace("<br>", "-", str_replace(" a ", " ", $text));
                $text = trim(preg_replace("/\s+/", " ", preg_replace("/&amp;#[0-9]+;/", "", $text)));
                $item["item"] = $text;
                if (!preg_match_all("/&pound;([0-9\.]+)/", $cells[1]->plaintext, $prices)) {
                    preg_match_all("/&pound;([0-9\.]+)/", $text, $prices);
                    $item["item"] = preg_replace("/\s*-?\s*&pound;([0-9\.]+)/", "", $text);
                }
                if (count($prices) >= 2 && count($prices[1])) {
                    $item["price"] = $prices[1];
                }
                array_push($list, $item);
            }
            $day[$sets[$x]] = $list;
            $x++;
        }
    }
    if (count($day) > 1) {
        array_push($root["menus"], $day);
    }
}
print(json_encode($root));
