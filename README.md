Introduction
============

This is a small PHP script to scrape the [Robinson College website](https://www.robinson.cam.ac.uk) for [Garden Restaurant menus](https://www.robinson.cam.ac.uk/college-life/garden-restaurant-menu).


Requirements
============

* PHP 5.0+
* the [Simple HTML DOM Parser](http://simplehtmldom.sourceforge.net/) at `~/lib/html_parse.php`
